public class TemperatureConverter{
    public static double celsiusToFahrenheit(double celsius) {
	return (celsius * 9 / 5) + 32;}
    public static double fahrenheitToCelsius(double fahrenheit) {
	return (fahrenheit - 32) * 5 / 9;}

    public static void printFeelingOnTemperature(double temperature){
	// handle 71 and below
	if(temperature <= 71){
	    p("too low");
	}
	
    }
    public static void p(String s){
	System.out.println(s);}
    public static void printFeelingOnTemperature(double temperature) {
	if (temperature <= 71) {
	    p("\nThe temp is " + temperature + ", It feels too cold");
	}

	if (temperature > 71 && temperature < 75) {
	    p("\nThe temp is " + temperature + ", It feels just right");
	}
	if (temperature >= 75) {
	    p("\nThe temp is " + temperature + ", It feels too warm");
	}
    }


    public static void main(String[] args){
	double temperature;
	System.out.println("hello world!");

	System.out.println("100 degrees celsius = "
			   + celsiusToFahrenheit(100)
			   + " degrees fahrenheit");

	System.out.println("44 degrees celsius = "
			   + celsiusToFahrenheit(44)
			   + " degrees fahrenheit");

	System.out.println("100 degrees fahrenheit = "
			   + fahrenheitToCelsius(100)
			   + " degrees celsius");


	/*
	  first, get the hand written test cases to work.

	  then,
	  use a for loop to print out every temp and how you feel about
	  it, for all integer temps between 60 and 90
	*/
	/*
	  71 and below, say "it feels too cold"
	  75 and above, say "it feels too warm"
	  in between, say "it feels just right"
	*/
	temperature = 55;
	printFeelingOnTemperature(temperature);
	// => "the temp is 55, and it feels too cold"

	temperature = 70;
	printFeelingOnTemperature(temperature);
	// => "the temp is 70, and it feels too cold"

	temperature = 72;
	printFeelingOnTemperature(temperature);
	// => "the temp is 72, and it feels just right"

	temperature = 76;
	printFeelingOnTemperature(temperature);
	// => "the temp is 76, it feels too warm"

	temperature++;
	temperature = temperature + 1;
	temperature += 1;
    }}
